<?php

namespace Drupal\Tests\commerce_product_tax_exempt\Functional;

use Drupal\Tests\commerce\Functional\CommerceBrowserTestBase;

/**
 * Class TaxTypeConditionsFormTest.
 *
 * @package Drupal\Tests\commerce_product_tax_exempt\Functional
 *
 * @group commerce_product_tax_exempt
 */
class TaxTypeConditionsFormTest extends CommerceBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'commerce',
    'commerce_product',
    'commerce_tax',
    'commerce_product_tax',
    'commerce_product_tax_exempt',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge([
      'administer commerce_tax_type',
    ], parent::getAdministratorPermissions());
  }

  /**
   * Test condition element on the form.
   */
  public function testFormElement() {
    $this->drupalGet('admin/commerce/config/tax-types/add');
    $edit = [
      'label' => 'Tax type',
      'id' => 'tax_type',
      'plugin' => 'custom',
      'configuration[custom][rates][0][rate][label]' => 'custom rate',
      'status' => 1,
    ];
    $this->assertText('Conditions');
    $this->drupalPostForm('admin/commerce/config/tax-types/add', $edit, 'Save');
    $this->assertText('Saved the Tax type tax type');
  }

}
