<?php

namespace Drupal\Tests\commerce_product_tax_exempt\Unit;

use Drupal\commerce_product_tax_exempt\Service\CommerceProductTaxExemptManager;
use Drupal\commerce_tax\Entity\TaxType;
use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;

/**
 * Class TaxExemptManagerTest.
 *
 * @package Drupal\Tests\commerce_product_tax_exempt\Unit
 *
 * @group commerce_product_tax_exempt
 */
class TaxExemptManagerTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $container = new ContainerBuilder();
    $taxExemptManager = new CommerceProductTaxExemptManager();
    $container->set('commerce_product_tax_exempt.manager', $taxExemptManager);
    \Drupal::setContainer($container);
  }

  /**
   * Checks if the service is created in the Drupal context.
   */
  public function testTaxExemptManager() {
    $this->assertNotNull(\Drupal::service('commerce_product_tax_exempt.manager'));
  }

  /**
   * Test fetching conditions from the tax type.
   */
  public function testFetchConditions() {
    $tax_type = new TaxType(['plugin' => 'default'], 'default');
    $conditions_to_save = [
      [
        'plugin' => 'test_plugin',
        'configuration' => Json::encode(['user_role' => 'admin', 'custom_config' => 'test']),
      ],
    ];
    $expected_conditions = [
      [
        'plugin' => 'test_plugin',
        'configuration' => ['user_role' => 'admin', 'custom_config' => 'test'],
      ],
    ];

    $tax_type->setThirdPartySetting('commerce_product_tax_exempt', 'conditions', $conditions_to_save);
    $saved_conditions = \Drupal::service('commerce_product_tax_exempt.manager')->getTaxTypeConditions($tax_type);
    $this->assertArrayEquals($expected_conditions, $saved_conditions);
  }

}
