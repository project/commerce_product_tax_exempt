<?php

namespace Drupal\commerce_product_tax_exempt\Service;

use Drupal\commerce_tax\Entity\TaxTypeInterface;
use Drupal\Component\Serialization\Json;

/**
 * Class CommerceProductTaxExemptManager.
 */
class CommerceProductTaxExemptManager {

  /**
   * Get conditions from tax type.
   */
  public function getTaxTypeConditions(TaxTypeInterface $tax_type) {
    $conditions = $tax_type->getThirdPartySetting('commerce_product_tax_exempt', 'conditions') ?: [];
    foreach ($conditions as &$condition) {
      $condition['configuration'] = Json::decode($condition['configuration']);
    }

    return $conditions;
  }

}
