<?php

namespace Drupal\commerce_product_tax_exempt\Resolver;

use Drupal\commerce\ConditionManagerInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_product_tax\Resolver\TaxRateResolver;
use Drupal\commerce_product_tax_exempt\Service\CommerceProductTaxExemptManager;
use Drupal\commerce_tax\TaxRate;
use Drupal\commerce_tax\TaxZone;
use Drupal\Core\Site\Settings;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Class ConditionTaxRate.
 */
class ConditionTaxRateResolver extends TaxRateResolver {

  /**
   * Commerce condition manager.
   *
   * @var \Drupal\commerce\ConditionManagerInterface
   */
  protected $conditionManager;

  /**
   * Exempt tax manager.
   *
   * @var \Drupal\commerce_product_tax_exempt\Service\CommerceProductTaxExemptManager
   */
  protected $exemptTaxManager;

  /**
   * ConditionTaxRate constructor.
   */
  public function __construct(ConditionManagerInterface $condition_manager, CommerceProductTaxExemptManager $exempt_tax_manager) {
    $this->conditionManager = $condition_manager;
    $this->exemptTaxManager = $exempt_tax_manager;
  }

  /**
   * Check if our tax types have saved conditions.
   */
  public function apply(TaxZone $zone, OrderItemInterface $order_item, ProfileInterface $customer_profile) {
    $tax_conditions = $this->exemptTaxManager->getTaxTypeConditions($this->taxType);
    if ($tax_conditions) {
      foreach ($tax_conditions as $tax_condition) {
        /** @var \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface $condition_plugin */
        $condition_plugin = $this->conditionManager->createInstance($tax_condition['plugin'], $tax_condition['configuration']);
        if (!$condition_plugin->evaluate($order_item)) {
          return FALSE;
        }
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve(TaxZone $zone, OrderItemInterface $order_item, ProfileInterface $customer_profile) {
    $rate = NULL;
    $conditions_only = Settings::get('commerce_product_tax_exempt_check_conditions_only', FALSE);
    if ($conditions_only) {
      $tax_type_configs = $this->taxType->getPluginConfiguration();
      if (!empty($tax_type_configs['rates'][0]['id'])) {
        $type_rate_id = $tax_type_configs['rates'][0]['id'];
        $rate = $zone->getRate($type_rate_id);
      }
    }
    else {
      $rate = parent::resolve($zone, $order_item, $customer_profile);
    }

    if ($rate instanceof TaxRate) {
      if (!$this->apply($zone, $order_item, $customer_profile)) {
        return static::NO_APPLICABLE_TAX_RATE;
      }
    }

    return $rate;
  }

}
