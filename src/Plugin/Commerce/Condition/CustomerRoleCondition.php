<?php

namespace Drupal\commerce_product_tax_exempt\Plugin\Commerce\Condition;

use Drupal\commerce_order\Plugin\Commerce\Condition\OrderCustomerRole;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class ExemptCondition.
 *
 * @CommerceCondition(
 *   id = "customer_role",
 *   label = @Translation("Customer role"),
 *   category = @Translation("Customer"),
 *   parent_entity_type = "commerce_tax_type",
 *   entity_type = "commerce_order_item",
 * )
 */
class CustomerRoleCondition extends OrderCustomerRole {

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $this->assertEntity($entity);
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
    $order_item = $entity;
    $customer = $order_item->getOrder()->getCustomer();

    return (bool) array_intersect($this->configuration['roles'], $customer->getRoles());
  }

}
