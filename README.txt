CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides conditions for the product tax exempt.
Adds UI for selecting conditions for Product Tax.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/commerce_product_tax_exempt

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/commerce_product_tax_exempt


REQUIREMENTS
------------

* Commerce Product Tax (https://www.drupal.org/project/commerce_product_tax):
  Provides a user interface for selecting applicable tax rates on the product
  variation.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * Mykhailo Gurei (ozin) - https://www.drupal.org/u/ozin
